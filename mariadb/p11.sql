-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: flota
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB-0+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `estado_reserva`
--

DROP TABLE IF EXISTS `estado_reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_reserva` (
  `id_estado_reserva` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_tipo_estado_reserva` int(11) NOT NULL,
  `id_reserva` bigint(20) NOT NULL,
  `fecha` date NOT NULL,
  `actual` bit(1) NOT NULL,
  `observaciones` text,
  `responsable` varchar(155) NOT NULL,
  PRIMARY KEY (`id_estado_reserva`),
  KEY `id_tipo_estado_reserva` (`id_tipo_estado_reserva`),
  KEY `id_reserva` (`id_reserva`),
  CONSTRAINT `estado_reserva_ibfk_1` FOREIGN KEY (`id_tipo_estado_reserva`) REFERENCES `tipo_estado_reserva` (`id_tipo_estado_reserva`),
  CONSTRAINT `estado_reserva_ibfk_2` FOREIGN KEY (`id_reserva`) REFERENCES `reserva` (`id_reserva`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_reserva`
--

LOCK TABLES `estado_reserva` WRITE;
/*!40000 ALTER TABLE `estado_reserva` DISABLE KEYS */;
INSERT INTO `estado_reserva` VALUES (1,1,1,'2018-12-12','','','Galactus'),(2,2,7,'2018-09-20','','C-va al mar','Patricio'),(3,2,4,'2018-08-18','','Pokimino de agua','RED'),(4,1,3,'2018-07-11','','Atomo','Eintens'),(5,1,11,'2018-09-19','','atrapa a zelda','LINK'),(6,1,10,'2018-08-19','','Miedo Ridley','Yakamoto');
/*!40000 ALTER TABLE `estado_reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado_vehiculo`
--

DROP TABLE IF EXISTS `estado_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado_vehiculo` (
  `id_estado_vehiculo` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_tipo_estado_vehiculo` int(11) NOT NULL,
  `id_vehiculo` bigint(20) NOT NULL,
  `fecha` date NOT NULL,
  `estado_actual` bit(1) NOT NULL,
  PRIMARY KEY (`id_estado_vehiculo`),
  KEY `id_tipo_estado_vehiculo` (`id_tipo_estado_vehiculo`),
  KEY `id_vehiculo` (`id_vehiculo`),
  CONSTRAINT `estado_vehiculo_ibfk_1` FOREIGN KEY (`id_tipo_estado_vehiculo`) REFERENCES `tipo_estado_vehiculo` (`id_tipo_estado_vehiculo`) ON DELETE CASCADE,
  CONSTRAINT `estado_vehiculo_ibfk_2` FOREIGN KEY (`id_vehiculo`) REFERENCES `vehiculo` (`id_vehiculo`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado_vehiculo`
--

LOCK TABLES `estado_vehiculo` WRITE;
/*!40000 ALTER TABLE `estado_vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `estado_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(155) NOT NULL,
  `descripcion` text,
  `activo` bit(1) NOT NULL,
  PRIMARY KEY (`id_marca`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'Alfa Romero','Marca Italiana',''),(2,'Aston Martin','Marca Britanica',''),(3,'Abarth','Compañia Italiana',''),(4,'Audi','Marca Italiana',''),(5,'Bentley','Marca Inglesa',''),(6,'BMW','Marca Alemana',''),(7,'Cadillac','Marca Estadunidense',''),(8,'Chevrolet','Marca Estadunidense',''),(9,'Ferrari','Marca Italiana',''),(10,'Ford','Marca Estadounidense',''),(11,'infiniti','Marca Japonesa',''),(12,'Honda','Compañia Japonesa',''),(13,'Dodge','Marca Estadounidense',''),(14,'Subaru','Marca Japonesa',''),(15,'Acura','Japonesa',''),(16,'Lexus','Marca Japonesa',''),(17,'Mazda','Marca Japonesa',''),(18,'Jeep','Marca Estadounidense',''),(19,'Jaguar','Marca Inglesa',''),(20,'Isuzu','Marca Japonesa',''),(21,'Opel','Empresa Alemana',''),(22,'Toyota','Empresa de Japon',''),(23,'KIA','Empresa de Corea del SUr',''),(24,'Nissan','Marca Japonesa',''),(25,'Hyundai','Marca de Corea del Sur',''),(26,'Mitsubishi','Marca Japonesa',''),(27,'suzuki','Marca Japonesa',''),(28,'Mini','Britanica','');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo`
--

DROP TABLE IF EXISTS `modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo` (
  `id_modelo` int(11) NOT NULL AUTO_INCREMENT,
  `id_marca` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `anio` int(11) DEFAULT NULL,
  `id_tipo_vehiculo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_modelo`),
  KEY `id_marca` (`id_marca`),
  KEY `id_tipo_vehiculo` (`id_tipo_vehiculo`),
  CONSTRAINT `modelo_ibfk_1` FOREIGN KEY (`id_marca`) REFERENCES `marca` (`id_marca`),
  CONSTRAINT `modelo_ibfk_2` FOREIGN KEY (`id_tipo_vehiculo`) REFERENCES `tipo_vehiculo` (`id_tipo_vehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo`
--

LOCK TABLES `modelo` WRITE;
/*!40000 ALTER TABLE `modelo` DISABLE KEYS */;
INSERT INTO `modelo` VALUES (1,23,'Sedona',2015,1),(2,17,'Mazda 5',2015,1),(3,22,'Sienna',2008,1),(4,12,'Odyssey EX-L',2009,1),(5,24,'Quest',2015,1),(6,13,'Gran Caravan',2012,1),(7,12,'Civic',2012,2),(8,23,'Rio',2013,2),(9,24,'Verssa',2016,2),(10,22,'Scion',2014,2),(11,24,'Sentra',2012,2),(12,8,'spark',2013,2),(13,25,'Accent',2015,2),(14,26,'Lancer',2016,1),(15,27,'Swift',2001,2),(16,25,'Elantra',2014,3),(17,10,'Ka top pulse',2014,3),(18,6,'bmw',2016,3),(19,8,'Astra 3P GLS',1999,3),(20,17,'MX-6',1994,3),(21,4,'A5',2016,3),(22,12,'Prelude 2.2 VTi Aut Secuencial',2,3),(23,28,'Cooper',2018,3),(24,27,'Fun 1.4L 3P',2010,3),(25,10,'Ka Fly Plus 1.0',2012,3),(26,14,'WRX',2016,4),(27,4,'RS3 sportback',2011,4),(28,27,'Otro',2012,4),(29,8,'Sonic',2015,4),(30,1,'Guilieta',2010,3),(31,10,'Mustang',2015,5),(32,26,'Eclipse',2008,5),(33,28,'Cooper S',2005,5),(34,22,'Celica',1986,5),(35,17,'Miata Mx5',2010,5),(36,6,'Z6 Standart',1998,5),(37,18,'Wrangler',2009,5),(38,10,'Escape',2007,6),(39,17,'CX',2012,6),(40,22,'R4VA',2016,6),(41,24,'Rogue',2013,6),(42,23,'Sedona',2013,6),(43,6,'325',2012,6),(44,22,'Corolla',2004,6),(45,13,'Caravan',1997,6),(46,18,'Cherokee',2014,7),(47,22,'HIlux',2017,7),(48,24,'Frontier',2008,7),(49,22,'4Runner',2007,7),(50,4,'Q7',2005,7),(51,13,'Ram SRT-10',2009,7),(52,10,'Ranger',2010,8),(53,24,'Navarra',2011,8);
/*!40000 ALTER TABLE `modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelo_parte`
--

DROP TABLE IF EXISTS `modelo_parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelo_parte` (
  `id_modelo_parte` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_modelo` int(11) NOT NULL,
  `id_parte` int(11) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_modelo_parte`),
  KEY `id_modelo` (`id_modelo`),
  KEY `id_parte` (`id_parte`),
  CONSTRAINT `modelo_parte_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `modelo` (`id_modelo`) ON DELETE CASCADE,
  CONSTRAINT `modelo_parte_ibfk_2` FOREIGN KEY (`id_parte`) REFERENCES `parte` (`id_parte`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelo_parte`
--

LOCK TABLES `modelo_parte` WRITE;
/*!40000 ALTER TABLE `modelo_parte` DISABLE KEYS */;
/*!40000 ALTER TABLE `modelo_parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parte`
--

DROP TABLE IF EXISTS `parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parte` (
  `id_parte` int(11) NOT NULL AUTO_INCREMENT,
  `id_sub_tipo_parte` int(11) NOT NULL,
  `nombre` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_parte`),
  KEY `id_sub_tipo_parte` (`id_sub_tipo_parte`),
  CONSTRAINT `parte_ibfk_1` FOREIGN KEY (`id_sub_tipo_parte`) REFERENCES `sub_tipo_parte` (`id_sub_tipo_parte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parte`
--

LOCK TABLES `parte` WRITE;
/*!40000 ALTER TABLE `parte` DISABLE KEYS */;
/*!40000 ALTER TABLE `parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recorrido`
--

DROP TABLE IF EXISTS `recorrido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recorrido` (
  `id_recorrido` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_reserva` bigint(20) NOT NULL,
  `coordenada_inicial` varchar(155) NOT NULL,
  `coordenada_final` varchar(155) NOT NULL,
  `fecha_desde` date NOT NULL,
  `fecha_hasta` date NOT NULL,
  `hora_desde` time NOT NULL,
  `hora_hasta` time NOT NULL,
  PRIMARY KEY (`id_recorrido`),
  KEY `id_reserva` (`id_reserva`),
  CONSTRAINT `recorrido_ibfk_1` FOREIGN KEY (`id_reserva`) REFERENCES `viaje` (`id_reserva`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recorrido`
--

LOCK TABLES `recorrido` WRITE;
/*!40000 ALTER TABLE `recorrido` DISABLE KEYS */;
/*!40000 ALTER TABLE `recorrido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reserva`
--

DROP TABLE IF EXISTS `reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reserva` (
  `id_reserva` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_vehiculo` bigint(20) NOT NULL,
  `cliente` varchar(255) DEFAULT NULL,
  `unidad` varchar(255) DEFAULT NULL,
  `fecha_reserva` date NOT NULL,
  `fecha_solicitada` date NOT NULL,
  `id_tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_reserva`),
  KEY `id_vehiculo` (`id_vehiculo`),
  KEY `id_tipo_usuario` (`id_tipo_usuario`),
  CONSTRAINT `reserva_ibfk_1` FOREIGN KEY (`id_vehiculo`) REFERENCES `vehiculo` (`id_vehiculo`),
  CONSTRAINT `reserva_ibfk_2` FOREIGN KEY (`id_tipo_usuario`) REFERENCES `tipo_usuario` (`id_tipo_usuario`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reserva`
--

LOCK TABLES `reserva` WRITE;
/*!40000 ALTER TABLE `reserva` DISABLE KEYS */;
INSERT INTO `reserva` VALUES (1,1,'Kirby','uno','2018-12-12','2018-01-01',1),(2,2,'Pikachu','UNO','2018-08-12','2018-09-20',2),(3,3,'Quarks','1','2018-07-11','2018-09-22',3),(4,4,'Quagsire','1','2018-08-18','2018-09-29',1),(5,7,'Lucas','1','2018-09-11','2018-10-01',2),(6,9,'Psyduck','1','2018-09-12','2018-10-12',2),(7,11,'Esponja','1','2018-09-20','2018-11-11',1),(8,7,'Mendelevio	','1','2018-08-25','2018-09-29',3),(9,8,'Maycol','1','2018-09-12','2018-12-12',1),(10,11,'Samus','1','2018-08-19','2018-09-30',3),(11,6,'Zelda','1','2018-09-19','2018-12-01',1);
/*!40000 ALTER TABLE `reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_tipo_parte`
--

DROP TABLE IF EXISTS `sub_tipo_parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_tipo_parte` (
  `id_sub_tipo_parte` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_parte` int(11) NOT NULL,
  `nombre` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_sub_tipo_parte`),
  KEY `id_tipo_parte` (`id_tipo_parte`),
  CONSTRAINT `sub_tipo_parte_ibfk_1` FOREIGN KEY (`id_tipo_parte`) REFERENCES `tipo_parte` (`id_tipo_parte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_tipo_parte`
--

LOCK TABLES `sub_tipo_parte` WRITE;
/*!40000 ALTER TABLE `sub_tipo_parte` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_tipo_parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_reserva`
--

DROP TABLE IF EXISTS `tipo_estado_reserva`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_reserva` (
  `id_tipo_estado_reserva` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `indica_aprobacion` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_tipo_estado_reserva`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_reserva`
--

LOCK TABLES `tipo_estado_reserva` WRITE;
/*!40000 ALTER TABLE `tipo_estado_reserva` DISABLE KEYS */;
INSERT INTO `tipo_estado_reserva` VALUES (1,'Activo/Aprobado','','','Aprueba  estados ed vehiculo'),(2,'Desactivo/Desaprobado','','','No esta aprobado');
/*!40000 ALTER TABLE `tipo_estado_reserva` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_estado_vehiculo`
--

DROP TABLE IF EXISTS `tipo_estado_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_estado_vehiculo` (
  `id_tipo_estado_vehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text NOT NULL,
  `no_disponible` bit(1) NOT NULL,
  PRIMARY KEY (`id_tipo_estado_vehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_estado_vehiculo`
--

LOCK TABLES `tipo_estado_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_estado_vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_estado_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_parte`
--

DROP TABLE IF EXISTS `tipo_parte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_parte` (
  `id_tipo_parte` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_tipo_parte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_parte`
--

LOCK TABLES `tipo_parte` WRITE;
/*!40000 ALTER TABLE `tipo_parte` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_parte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(155) NOT NULL,
  `app_id` varchar(155) NOT NULL,
  `activo` bit(1) NOT NULL,
  `observaciones` text,
  PRIMARY KEY (`id_tipo_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'Corriente','DUI','','Es el jaimito '),(2,'Nuevo','Carnet','','Es de plaza sesamo'),(3,'Prodigo','NIT','','Es chepet :v');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_vehiculo`
--

DROP TABLE IF EXISTS `tipo_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_vehiculo` (
  `id_tipo_vehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `peso_min_lbs` float DEFAULT NULL,
  `peso_max_lbs` float DEFAULT NULL,
  `cantidad_ejes` int(11) DEFAULT NULL,
  `activo` bit(1) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_tipo_vehiculo`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_vehiculo`
--

LOCK TABLES `tipo_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_vehiculo` DISABLE KEYS */;
INSERT INTO `tipo_vehiculo` VALUES (1,'Minivan',4000,7000,2,'','Pos minivan'),(2,'sedán',2431.6,2502.2,2,'','Auto clasico'),(3,'Coupé',2667.6,3262.8,2,'','Auto pequeño'),(4,'Hatchback',2156.1,2332.4,2,'','Auto de una sola puerta'),(5,'Descapotable',2336.9,2738.1,2,'','Auto sin techo'),(6,'Familiar',2601.4,3880.1,2,'','Vehiculo para toda la familia'),(7,'TodoTerreno',4010.3,4030.05,2,'\0','Un auto TodoTerreno'),(8,'Pick-up',3282.6,3825.2,2,'','Pikack :v');
/*!40000 ALTER TABLE `tipo_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiculo` (
  `id_vehiculo` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_chasis` varchar(255) NOT NULL,
  `numero_vin` varchar(255) NOT NULL,
  `id_modelo` int(11) NOT NULL,
  `numero_placa` varchar(155) DEFAULT NULL,
  `asientos` int(11) DEFAULT NULL,
  `color` varchar(155) NOT NULL,
  PRIMARY KEY (`id_vehiculo`),
  KEY `id_modelo` (`id_modelo`),
  CONSTRAINT `vehiculo_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `modelo` (`id_modelo`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
INSERT INTO `vehiculo` VALUES (1,'12','12',1,'1234',3,'Blue'),(2,'2135653','C3',7,'P358686',4,'Blanco'),(3,'21543','A23',34,'Ae2356',3,'Rojo'),(4,'12590','Aw23',9,'123f',5,'Verde'),(5,'10733','Q12',4,'Aq1468',5,'Lila'),(6,'1111','P11',11,'qw332',4,'Gris'),(7,'987654','yt64',49,'zx5433',5,'Negro'),(8,'76534','ZX34',52,'ra763',2,'Azul'),(9,'277952','AW479',45,'YT567',2,'Amarillo'),(10,'0759','Da9090',21,'qje31',4,'Celeste'),(11,'as31','po0987',12,'po098',4,'VIoleta');
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `viaje` (
  `id_reserva` bigint(20) NOT NULL,
  `fecha_desde` date NOT NULL,
  `fecha_hasta` int(11) NOT NULL,
  `kilometros` float NOT NULL,
  `motorista` varchar(155) DEFAULT NULL,
  `observaciones` text,
  `hora_salida` time NOT NULL,
  `hora_llegada` time NOT NULL,
  PRIMARY KEY (`id_reserva`),
  CONSTRAINT `viaje_ibfk_1` FOREIGN KEY (`id_reserva`) REFERENCES `reserva` (`id_reserva`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viaje`
--

LOCK TABLES `viaje` WRITE;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-20 11:34:32
